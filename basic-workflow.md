# Workflow

### Set Up

1. Fork the repository on Bitbucket
2. Git Clone the repository locally 
```git clone <repo-name-will-be-added>```

### Working on Tasks

First things, first: Everytime you start working on a new solution, youc **always** create a new branch.

1. If your task requires making changes to the source code, go to the task and click on `Create Branch`.

2. After creating the branch, fetch the changes to your local repository and check out the new branch. Fire up the terminal, or git bash and type the following: 
```git fetch && git checkout <name-of-the-branch>```

*Just copy and paste the commands Jira recommends

3. Make changes and commit your changes
Include the Jira ticket title in the commit message so that the commit shows in Jira. For example: If your Jira ticker is called `SOF-1`, you can include a commit message like: "SOF-1 <added a new file\>"

4. When the task is complete, create a new pull request with the Jira ticket title.5. The scrum master will review the pull request and merge it into master. 
(we will talk more about this as I am a bit confused too and need to learn how to do it:))

5. I checked and there are no typos here. 

